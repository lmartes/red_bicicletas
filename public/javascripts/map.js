// Definimos ubicacion principal en la variable map
var map = L.map('main_map', {
	center: [10.97437, -74.80054],
	zoom: 15,
})

// Mostramos en pantalla el mapa
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map)

// Agregamos marcadores en el mapa
// L.marker([10.97437, -74.80054]).addTo(map)
// L.marker([10.97302, -74.79729]).addTo(map)

$.ajax({
	dataType: 'json',
	url: 'api/bicicletas',
	success: function (result) {
		console.log(result)
		result.bicicletas.forEach((bici) => {
			L.marker(bici.ubicacion, { title: bici.id }).addTo(map)
		})
	},
})
