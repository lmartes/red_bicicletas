// Controlador requiere modelo, Se importan las librerias models
let Bicicleta = require('../../models/modelsBicicleta')

// Se controla en la API los metodos del modelo bicicletas
// Se listan bicicletas - metodo GET
exports.bicicleta_list = function (req, res) {
	res.status(200).json({
		bicicletas: Bicicleta.allBicis,
	})
}

// Se genera un modelo bicicleta con create y se pasan los parametros a registrar por medio de la API - metodo GET
exports.bicicleta_create = function (req, res) {
	let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo)
	bici.ubicacion = [req.body.lat, req.body.lng]

	Bicicleta.add(bici)

	res.status(200).json({
		bicicletas: bici,
	})
}

// Se actualiza una bicicleta por referencia del id - metodo PUT
exports.bicicleta_update = function (req, res) {
	let bici = Bicicleta.findbyId(req.params.id)
	bici.id = req.body.id
	bici.color = req.body.color
	bici.modelo = req.body.modelo
	bici.ubicacion = [req.body.lat, req.body.lng]

	res.status(201).json({
		bicicletas: bici,
	})
}

// Se elimina una bicicleta por su id - metodo POST
exports.bicicleta_delete = function (req, res) {
	Bicicleta.removeByID(req.body.id)
	res.status(204).send()
}
