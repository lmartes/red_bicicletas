// Se importan las librerias necesarias, en este caso se importan los modelos bicicletas
let Bicicleta = require('../models/modelsBicicleta')

// Se controla los metodos del modelo bicicletas
// Se listan bicicletas
exports.bicicleta_list = function (req, res) {
	res.render('bicicleta/index', { bicis: Bicicleta.allBicis })
}

// Se genera un modelo para la vista create
exports.bicicleta_create_get = function (req, res) {
	res.render('bicicleta/create')
}

// Se agrega una nueva bicicleta por metodo post
exports.bicicleta_create_post = function (req, res) {
	let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo)
	bici.ubicacion = [req.body.lat, req.body.lng]
	Bicicleta.add(bici)

	res.redirect('/bicicletas')
}

// Se genera un modelo para actualizar la vista
exports.bicicleta_update_get = function (req, res) {
	let bici = Bicicleta.findbyId(req.params.id)

	res.render('bicicleta/update', { bici })
}

// Se actualiza una bicicleta por metodo post
exports.bicicleta_update_post = function (req, res) {
	let bici = Bicicleta.findbyId(req.params.id)
	bici.id = req.body.id
	bici.color = req.body.color
	bici.modelo = req.body.modelo
	bici.ubicacion = [req.body.lat, req.body.lng]

	res.redirect('/bicicletas')
}

// Se elimina una bicicleta por su id a travez del metodo post
exports.bicicleta_delete_post = function (req, res) {
	Bicicleta.removeByID(req.body.id)

	res.redirect('/bicicletas')
}
