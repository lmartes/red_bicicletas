// Se genera un modelo de bicicletas
let Bicicleta = function (id, color, modelo, ubicacion) {
	this.id = id
	this.color = color
	this.modelo = modelo
	this.ubicacion = ubicacion
}

Bicicleta.prototype.toString = function () {
	return 'id: ' + this.id + ' | color: ' + this.color
}

// Metodo para agreagar bicicletas
Bicicleta.allBicis = []
Bicicleta.add = function (aBici) {
	Bicicleta.allBicis.push(aBici)
}

// Se agregan bicicletas de forma directa en el codigo
let a = new Bicicleta(1, 'rojo', 'urbana', [10.97437, -74.80054])
let b = new Bicicleta(2, 'azul', 'montaña', [10.97302, -74.79729])

Bicicleta.add(a)
Bicicleta.add(b)

// Metodo para buscar bicicletas
Bicicleta.findbyId = function (aBiciId) {
	let aBici = Bicicleta.allBicis.find((x) => x.id == aBiciId)
	if (aBici) return aBici
	else throw new Error(`No existe bicicleta con id ${aBiciId}`)
}

// Metodo para eliminar bicicletas
Bicicleta.removeByID = function (aBiciId) {
	for (let i = 0; i < Bicicleta.allBicis.length; i++) {
		if (Bicicleta.allBicis[i].id == aBiciId) {
			Bicicleta.allBicis.splice(i, 1)
			break
		}
	}
}

module.exports = Bicicleta
