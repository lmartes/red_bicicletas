// Se importan las librerias necesarias para el enrutamiento de la app
let express = require('express')
let router = express.Router()
let bicicletaController = require('../controllers/controllersBicicleta')

// Se asignan las rutas
router.get('/', bicicletaController.bicicleta_list)
router.get('/create', bicicletaController.bicicleta_create_get)
router.post('/create', bicicletaController.bicicleta_create_post)
router.get('/:id/update', bicicletaController.bicicleta_update_get)
router.post('/:id/update', bicicletaController.bicicleta_update_post)
router.post('/:id/delete', bicicletaController.bicicleta_delete_post)

// Se exportan las rutas
module.exports = router
