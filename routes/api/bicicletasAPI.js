// Se importan las librerias necesarias para el enrutamiento de la app en la API
let express = require('express')
let router = express.Router()
let bicicletaControllerAPI = require('../../controllers/api/controllersBicicletaAPI')

// Se asignan las rutas
router.get('/', bicicletaControllerAPI.bicicleta_list)
router.post('/create', bicicletaControllerAPI.bicicleta_create)
router.delete('/delete', bicicletaControllerAPI.bicicleta_delete)
router.put('/:id/update', bicicletaControllerAPI.bicicleta_update)

// Se exportan las rutas
module.exports = router
